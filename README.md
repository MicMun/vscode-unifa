# UNIFA-Syntaxes

A collection of tools for Visual Studio Code to ease working in the UNIFA Environment.

## Features

### Syntax highlighting for

- trace.ufa files
- ufalistmsg output
- Message Catalog files, used by ufalistmsg
- .bs2log BS2000 access logs
- .SQL.LOG and .SQL.WV.LOG Maintain-DB logs
- UNIFA MLS (UNIFA Map/List/String), as written by Java 'toString()'
- tnsnames.ora (Oracle DBs)
- Fehlertext (.fel)

## Release Notes

### 0.0.5

Current development version with syntax highlighting.

# Author and License

Copyright 2021-2024 MicMun.

Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with the License. You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the specific language governing permissions and limitations under the License.
